<?php

declare(strict_types=1);

namespace designerei\ContaoCardBundle;

use designerei\ContaoCardBundle\DependencyInjection\ContaoCardExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ContaoCardBundle extends Bundle
{
    public function getContainerExtension(): ContaoCardExtension
    {
        return new ContaoCardExtension();
    }
}
