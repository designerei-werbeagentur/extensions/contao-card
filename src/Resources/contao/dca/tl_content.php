<?php

declare(strict_types=1);

$table = 'tl_content';

$GLOBALS['TL_DCA'][$table]['subpalettes']['addLink'] = 'url,target,linkTitle,titleText';
$GLOBALS['TL_DCA'][$table]['subpalettes']['addText'] = 'text';

$GLOBALS['TL_DCA'][$table]['palettes']['__selector__'][] = 'addLink';
$GLOBALS['TL_DCA'][$table]['palettes']['__selector__'][] = 'addText';

$GLOBALS['TL_DCA'][$table]['palettes']['card'] =
    '{type_legend},type,headline;'
    . '{text_legend},addText;'
    . '{image_legend},addImage;'
    . '{link_legend},addLink;'
    . '{template_legend:hide},customTpl;'
    . '{protected_legend:hide},protected;'
    . '{expert_legend:hide},guests,cssID;'
    . '{invisible_legend:hide},invisible,start,stop'
;

$GLOBALS['TL_DCA'][$table]['fields']['addLink'] = [
    'exclude'                 => true,
    'inputType'               => 'checkbox',
    'eval'                    => array('submitOnChange'=>true),
    'sql'                     => "char(1) NOT NULL default ''"
];

$GLOBALS['TL_DCA'][$table]['fields']['addText'] = [
    'exclude'                 => true,
    'inputType'               => 'checkbox',
    'eval'                    => array('submitOnChange'=>true),
    'sql'                     => "char(1) NOT NULL default ''"
];
