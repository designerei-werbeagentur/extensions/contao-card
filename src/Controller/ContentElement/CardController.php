<?php

declare(strict_types=1);

namespace designerei\ContaoCardBundle\Controller\ContentElement;

use Contao\ContentModel;
use Contao\CoreBundle\Controller\ContentElement\AbstractContentElementController;
use Contao\CoreBundle\ServiceAnnotation\ContentElement;
use Contao\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @ContentElement("card",
 *   category="texts",
 * )
 */
class CardController extends AbstractContentElementController
{
    protected function getResponse(Template $template, ContentModel $model, Request $request): ?Response
    {

        $template->text = \StringUtil::toHtml5($model->text);

        // Encode e-mail addresses
        if ($model->url) {
            if (strncmp($model->url, 'mailto:', 7) === 0) {
                $template->url = \StringUtil::encodeEmail($model->url);
            } else {
                $template->url = \StringUtil::ampersand($model->url);
            }
        }

        $template->text = \StringUtil::encodeEmail($model->text);

        // Use linkTitle instead of url
        if (!$model->linkTitle)
        {
            $model->linkTitle = $model->url;
        }

        // Override the link target
        if ($model->target)
        {
            $template->target = ' target="_blank" rel="noreferrer noopener"';
        }

        // Rename variables
        $template->href = $template->url;
        $template->link = $template->linkTitle;

        // Add an image
        if ($model->addImage && $model->singleSRC)
        {
            $objModel = \FilesModel::findByUuid($model->singleSRC);

            if ($objModel !== null && is_file(\System::getContainer()->getParameter('kernel.project_dir') . '/' . $objModel->path))
            {
                $model->singleSRC = $objModel->path;
                $template->addImageToTemplate($template, $model->row(), null, null, $objModel);
            }
        }

        return $template->getResponse();
    }
}
